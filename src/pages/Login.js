import { Form, Button } from "react-bootstrap";
import { useState, useEffect, useContext } from "react";
import { Navigate } from "react-router-dom";
import UserContext from "../UserContext";

export default function Login() {
   // Allow us to consume the User Context object and its properties
   const { user, setUser } = useContext(UserContext);

   // State hooks -> Store values of the input fields
   const [email, setEmail] = useState("");
   const [password1, setPassword1] = useState("");

   const [isActive, setIsActive] = useState(false);

   console.log(email);
   console.log(password1);

   useEffect(() => {
      // validation to enable register button when all fields is populated and both password an match
      if (email !== "" && password1 !== "") {
         setIsActive(true);
      } else {
         setIsActive(false);
      }
   }, [email, password1]);

   // function to simulate user registration
   function loginUser(e) {
      // prevents page redirection via form submission

      // Set the email of the authenticated user in the local storage
      /* 
         Syntax
         localStorage.setItem("propertyName", value)
      */
      localStorage.setItem("email", email);

      setUser({
         email: localStorage.getItem("email"),
      });

      setEmail("");
      setPassword1("");

      alert("Successfully log-in");
   }

   return user.email !== null ? (
      <Navigate to="/courses" />
   ) : (
      <Form onSubmit={(e) => loginUser(e)}>
         <h1 className="text-center">Login</h1>
         <Form.Group className="mb-3" controlId="userEmail">
            <Form.Label>Email address</Form.Label>
            <Form.Control
               type="email"
               placeholder="Enter email"
               value={email}
               onChange={(event) => setEmail(event.target.value)}
               required
            />
         </Form.Group>

         <Form.Group className="mb-3" controlId="password1">
            <Form.Label>Password</Form.Label>
            <Form.Control
               type="password"
               placeholder="Password"
               value={password1}
               onChange={(event) => setPassword1(event.target.value)}
               required
            />
         </Form.Group>

         {/* Conditional Rendering -> if active button is clickable -> if inactive button is not clickable */}

         {isActive ? (
            <Button variant="success" type="submit" controlId="submitBtn">
               Login
            </Button>
         ) : (
            <Button
               variant="success"
               type="submit"
               controlId="submitBtn"
               disabled
            >
               Login
            </Button>
         )}
      </Form>
   );
}
