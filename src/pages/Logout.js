import { useEffect, useContext } from "react";
import { Navigate } from "react-router-dom";
import UserContext from "../UserContext";

export default function Logout() {
   // Consume the UserContext object and destructure it to access the user state and unsetUser function from the ontext provider
   const { unsetUser, setUser } = useContext(UserContext);

   // Clear the localStorage of the users information
   unsetUser();

   // localStorage.clear();

   useEffect(() => {
      // Set the user state back to its original value
      setUser({ email: null });
   });

   return <Navigate to="/login" />;
}
