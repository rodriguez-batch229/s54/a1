import { Form, Button } from "react-bootstrap";
import { useState, useEffect, useContext } from "react";
import { Navigate } from "react-router-dom";
import UserContext from "../UserContext";

export default function Register() {
   // State hooks -> Store values of the input fields

   const [email, setEmail] = useState("");
   const [password1, setPassword1] = useState("");
   const [password2, setPassword2] = useState("");

   const [isActive, setIsActive] = useState(false);

   const { user, setUser } = useContext(UserContext);

   console.log(email);
   console.log(password1);
   console.log(password2);

   useEffect(() => {
      // validation to enable register button when all fields is populated and both password an match
      if (
         email !== "" &&
         password1 !== "" &&
         password2 !== "" &&
         password1 === password2
      ) {
         setIsActive(true);
      } else {
         setIsActive(false);
      }
   });

   // function to simulate user registration
   function registerUser(e) {
      // prevents page redirection via form submission
      // e.preventDefault();
      localStorage.setItem("email", email);

      setUser({
         email: localStorage.getItem("email"),
      });

      setEmail("");
      setPassword1("");
      setPassword2("");

      alert("Thank you for registering!");
   }

   return user.email !== null ? (
      <Navigate to="/courses" />
   ) : (
      <Form onSubmit={(e) => registerUser(e)}>
         <h1 className="text-center">Register</h1>
         <Form.Group className="mb-3" controlId="userEmail">
            <Form.Label>Email address</Form.Label>
            <Form.Control
               type="email"
               placeholder="Enter email"
               value={email}
               onChange={(event) => setEmail(event.target.value)}
               required
            />
            <Form.Text className="text-muted">
               We'll never share your email with anyone else.
            </Form.Text>
         </Form.Group>

         <Form.Group className="mb-3" controlId="password1">
            <Form.Label>Password</Form.Label>
            <Form.Control
               type="password"
               placeholder="Password"
               value={password1}
               onChange={(event) => setPassword1(event.target.value)}
               required
            />
         </Form.Group>
         <Form.Group className="mb-3" controlId="password2">
            <Form.Label>Verify Password</Form.Label>
            <Form.Control
               type="password"
               placeholder="Password"
               value={password2}
               onChange={(event) => setPassword2(event.target.value)}
               required
            />
         </Form.Group>

         {/* Conditional Rendering -> if active button is clickable -> if inactive button is not clickable */}

         {isActive ? (
            <Button variant="primary" type="submit" controlId="submitBtn">
               Register
            </Button>
         ) : (
            <Button
               variant="primary"
               type="submit"
               controlId="submitBtn"
               disabled
            >
               Register
            </Button>
         )}
      </Form>
   );
}
